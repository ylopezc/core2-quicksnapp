<?php
//@Author: Wilson Ramirez Zuluaga - Heiller Bonilla
//@date: 10/08/2015
//@Update: 07/12/2015
//Clase: Clase padre de Todos los controladores y gestiona vista, modelo
require_once (ROOTC . "claseBase" . DS . "seguridad.php");
abstract class Controlador extends seguridad
{

  public function __construct()
  {
  	  parent::__construct();
      $this->vista = new Vista(new Peticion);
      if(SESSION=="ON")
      {
        session_start();
      }
  }
   abstract function index();

   public function modelo($_modelo)
   {
     $_modelo = $_modelo . "Modelo";
     $_rutaModelo = ROOT . DS . "Modelo" . DS . $_modelo . ".php";
     if(is_readable($_rutaModelo))
     {
       require_once ($_rutaModelo);
       $_modeloN = new $_modelo;
       return $_modeloN;
     }
     else
     {
        require_once (ROOTC . "Error/errorModelo.phtml");
     }

   }
   public function clase($_clase)
   {
   		$claseInterna = $_clase . ".php";
		$_rutaClase = ROOT . DS. "CLASES" .DS . $claseInterna;
		if(is_readable($_rutaClase))
		{
			require_once ($_rutaClase);
			$_objetoClase = new $_clase();
		}
		else 
		{
			echo "Error 1005";
		}
		return $_objetoClase;
   }
   public function getArgumento()
   {
   	return $this->vista->getArgumento();
   }
}
?>
