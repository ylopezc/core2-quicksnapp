<?php
class Mysql extends Modelo
{
  protected $usuario, $clave, $host, $dbUsuario;
  public function __construct()
  {
    //Asignamos Variables
    $this->usuario = USUARIO;
    $this->clave = CLAVE;
    $this->host = HOST;
    $this->dbUsuario = DB;
    //Creamos la conexion a la base de datos
    $this->conexion = new mysqli($this->host, $this->usuario, $this->clave, $this->dbUsuario);
    $this->conexion->set_charset("utf8");
    if($this->conexion)
    {

    }
    else
    {
      require_once (ROOTC . "Error/errorConexion.phtml");
    }
  }
  //consultar($_tabla, $_campos=array(), $_condicion="", $_limite="")
  public function consultar($_tabla, $_campos=array(), $_condicion="", $_limite="")
  {
    $_cadena = "SELECT ";
    if(count($_campos) == 0)
    {
      $_cadena .= "* FROM " . $_tabla;
    }
    else
    {
      if($_campos[0]!="*")
      {
      foreach ($_campos as $_nombreCampo)
      {
        $_cadena .= $_nombreCampo . ", ";
      }
      $_cadena = substr($_cadena, 0, strlen($_cadena)-2) . " FROM " . $_tabla;
      }
      else
      {
         $_cadena .= "* FROM " . $_tabla;
      }
    }
    if($_condicion !="")
    {
      $_cadena .= " WHERE " . $_condicion;
    }
    if($_limite !="")
    {
      $_cadena .= " LIMIT " . $_limite;
    }
    $_consulta = $this->conexion->query($_cadena);
    $_consultaSalida =  $_consulta->fetch_all(MYSQLI_ASSOC);
    return $_consultaSalida;
  }
  //public function insertar($_tabla, $_post)
  public function insertar($_tabla, $_post)
  {
      $_cadena = "INSERT INTO ". $_tabla . "(";
      $_nombreCampos="";
      $_valoresCampos="";
      $_campos= $this->conexion->query("DESC " . $_tabla);
      $_camposArray= $_campos->fetch_all(MYSQLI_ASSOC);
      $_camposInternos;
      $_camposInternosMayuscula;
      foreach ($_camposArray as $llaveCampos => $valorCampos)
      {
         $_camposInternos[]= $valorCampos['Field'];
         $_camposInternosMayuscula[]= strtoupper($valorCampos['Field']);
      }
      //$_camposInternos son los campos que hay dentro de la tabla
      //Comparamos los campos de la tabla con respecto a lo enviado y creamos la cadena para insertar
      foreach ($_post as $llaveCamposF => $valorCamposF)
      {
        $_busquedaCampos = array_search(strtoupper($llaveCamposF), $_camposInternosMayuscula);
        if($_busquedaCampos!=null)
        {
          if($valorCamposF!= "")
          {
          $_nombreCampos .= $_camposInternos[$_busquedaCampos] . ", ";
          $_valoresCampos .= "'" . strtoupper(htmlspecialchars($valorCamposF)) . "'" . ", ";
          }
        }
      }
      $_nombreCampos = substr($_nombreCampos, 0, strlen($_nombreCampos)-2);
      $_valoresCampos = substr($_valoresCampos, 0, strlen($_valoresCampos)-2);
      $_cadena .= $_nombreCampos . ") VALUES(" . $_valoresCampos . ")";
      //echo $_cadena;
      $this->conexion->query($_cadena);
      return $this->conexion->insert_id;
  }
  //Consulta libre
  public function consultarlibre($_cadena)
  {
     $_consulta= $this->conexion->query($_cadena);
     return $_consulta->fetch_all(MYSQLI_ASSOC);
  }
  //Actualziar datos de una tabla actualizar($_tabla, $_campos, $_condicion)
  public function actualizar($_tabla, $_post, $_condicion)
  {
    $_cadena = "UPDATE ". $_tabla . " SET ";
    $_nombreCampos="";
    $_valoresCampos="";
    $_campos= $this->conexion->query("DESC " . $_tabla);
    $_camposArray= $_campos->fetch_all(MYSQLI_ASSOC);
    $_camposInternos;
    $_camposInternosMayuscula;
    foreach ($_camposArray as $llaveCampos => $valorCampos)
    {
       $_camposInternos[]= $valorCampos['Field'];
       $_camposInternosMayuscula[]= strtoupper($valorCampos['Field']);
    }
    //$_camposInternos son los campos que hay dentro de la tabla
    //Comparamos los campos de la tabla con respecto a lo enviado y creamos la cadena para insertar
    foreach ($_post as $llaveCamposF => $valorCamposF)
    {
      $_busquedaCampos = array_search(strtoupper($llaveCamposF), $_camposInternosMayuscula);
      if($_busquedaCampos!=null)
      {
        if($valorCamposF!= "")
        {
        $_nombreCampos .= $_camposInternos[$_busquedaCampos] . "= '" . $valorCamposF . "', ";

        }
      }
    }
    $_nombreCampos = substr($_nombreCampos, 0, strlen($_nombreCampos)-2);

    $_cadena .= $_nombreCampos . " WHERE " . $_condicion;
    //echo $_cadena;
    $this->conexion->query($_cadena);
  }
}


 ?>
