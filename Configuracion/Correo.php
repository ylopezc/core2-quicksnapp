<?php
//Case para envio de correo electronico
require_once (ROOTC . "claseBase" . DS . "PHPMailer-master" . DS ."PHPMailerAutoload.php");
class Correo 
{
	
	protected $mail;
	public function __construct()
	{
		$this->mail = new PHPMailer;
		$this->mail->SMTPDebug = 3;                               // Enable verbose debug output
	}
	public function modo($host, $usuario, $clave)
	{
		$this->mail->isSMTP();
		$this->mail->Host = $host;
		$this->mail->SMTPAuth = true;                   // Enable SMTP authentication
		$this->mail->Username = $usuario;                 // SMTP username
		$this->mail->Password = $clave;                 // SMTP password
		$this->mail->SMTPSecure = 'ssl';                // Enable TLS encryption, `ssl` also accepted
		$this->mail->Port = 465; 						// TCP port to connect to 587 o 465
		
	}
	public function configurarCorreo($asunto, $mensaje, $mensajeAlterno="")
	{
		$this->mail->Subject = $asunto;
		$this->mail->Body    = $mensaje;
		$this->mail->AltBody = $mensajeAlterno;
	}
	public function enviarCorreo ($emisor, $destino, $nombre ="", $copia="", $copiaOculta="", $adjunto=array())
	{
		$this->mail->setFrom($emisor, 'Administrador');
		//Si hay varios destinos se adiciona por array
		foreach ($destino as $llaveDestino)
		{
			
			$this->mail->addAddress($llaveDestino);//$this->mail->addAddress($destino, $nombre);
		}
		$this->mail->addReplyTo($emisor, 'Información General');
		if($copia != "")
		$this->mail->addCC($copia);
		if($copiaOculta != "")
		$this->mail->addBCC($copiaOculta);
		$this->mail->isHTML(true);                                  // Set email format to HTML
		if(!$this->mail->send()) 
		{
			echo 'El Mensaje no se pudo enviar.';
			echo 'Mensaje de Error: ' . $this->mail->ErrorInfo;
		} 
		else 
		{
			 'Mensaje enviado';
		}
	}
	public function adjunto($rutaAdjunto= array())
	{
		//Ruta de Documentos adjuntos
		foreach($rutaAdjunto as $llaveRutaAdjunto)
		{
			$this->mail->addAttachment($llaveRutaAdjunto);
		}
		      
		
	}
	
}