<?php
//@Author: Wilson Ramirez Zuluaga - Heiller Bonilla
//@date: 10/08/2015
//Clase: Ejecutar y Crear las rutas de Controlador, Metodo, Argumento
class Inicio
{

    public function Ejecutar(Peticion $_peticion)
    {
      if($_peticion->getControlador() == "")
      {
      $_cargaControlador = CONTROLADOR_DEFECTO;
    }
    else {
      $_cargaControlador = $_peticion->getControlador();
    }
       $_rutaControlador = ROOT . DS . "Controlador" . DS . $_cargaControlador . "Controlador.php";
       $_metodo = $_peticion->getMetodo();
       $_argumento = $_peticion->getArgumento();
        $_rutaControlador;
       if(is_readable($_rutaControlador))
       {
         $_controlador = $_cargaControlador . "Controlador";
         require_once ($_rutaControlador);
         $_controlador = new $_controlador;
        if(is_callable(array($_controlador,$_metodo)))
        {
          $_metodo = $_peticion->getMetodo();
             if(isset($_argumento))
             {
                call_user_func(array($_controlador, $_metodo),$_argumento);
              }
              else
              {
              call_user_func(array($_controlador, $_metodo));
               }
        }
        else
        {
          $_metodo= "index";
          call_user_func(array($_controlador, $_metodo));
        }
       }
       else
       {
         require_once (ROOTC . "Error/error404.phtml");
         //throw new Exception("Controlador No Encontrado");
       }


    }
}
?>
