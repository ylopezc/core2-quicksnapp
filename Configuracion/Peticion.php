<?php
//@Author: Wilson Ramirez Zuluaga - Heiller Bonilla
//@date: 10/08/2015
//Clase: Encargada de Resolver la Peticion de los Clientes en la URL
class Peticion
{
  private $_controlador, $_metodo, $_argumento, $_url;

  public function __construct()
  {

    if($_GET['url']!= "")
    {
    $this->_url= explode("/",$_GET['url']);
    $this->_controlador = array_shift($this->_url);
    $this->_metodo = array_shift($this->_url);
    $this->_argumento = $this->_url;

  }
  else
  {
    $this->_controlador = CONTROLADOR_DEFECTO;
    $this->_metodo = "";
    $this->_argumento = array();
  }
  }
  public function getControlador()
  {
    return $this->_controlador;
  }
  public function getMetodo()
  {
    return $this->_metodo;
  }
  public function getArgumento()
  {
    return $this->_argumento;
  }

}
?>
