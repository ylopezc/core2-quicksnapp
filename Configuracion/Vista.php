<?php
/*
 @Author Wilson Ramirez Zuluaga
 @date 12/03/2014
 @version 1.0
 @todo Configuracion de las vistas para el Usuario
 */
class Vista
{
  private $_controlador;
  public function __construct(Peticion $_peticion)
  {
    $this->_controlador = $_peticion->getControlador();
    $this->_arg = $_peticion->getArgumento();
  }
  public function renderizar($_vista="",$_layout="")
  {
    $_rutaVista = ROOT . DS . "Vista" . DS. $this->_controlador . DS;
    $_rutaLayout = ROOT . DS . "Layout" . DS;

    if($_layout == "")
    {
      $_layout1 = LAYOUT;
    }
    else
    {
      $_layout1 = $_layout;
    }
    if($_vista == "")
    {
       $_vista = $_rutaVista . VISTA . ".phtml";
    }
    else
    {
         $_vista = $_rutaVista . $_vista . ".phtml";
    }
    require_once ($_rutaLayout . $_layout1 . DS . "head.php");
    require_once ($_vista);
    require_once ($_rutaLayout . $_layout1 . DS . "footer.php");
    }
  public function getArgumento()
  {
    return $this->_arg;
  }
}


?>
