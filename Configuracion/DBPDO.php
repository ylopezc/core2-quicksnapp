<?php
/*
 @Author Wilson Ramirez Zuluaga
 @date 12/03/2016
 @version 1.0
 @todo Configuracion de clase PDO para acceso rápido
 */

abstract class DBPDO
{
		protected  $host = HOST;
		protected  $usuario = USUARIO;
		protected  $clave = CLAVE;
		protected  $gestor = GESTOR;
		protected  $db = DB;
		protected  $conexion;
		protected  $_cadenaConexion;
		
	public function __construct()
	{
		try
		{
			$this->_cadenaConexion = $this->gestor . ":host=" . $this->host . ";dbname=" . $this->db;
			$this->conexion = new PDO($this->_cadenaConexion, $this->usuario, $this->clave, array(PDO::ATTR_PERSISTENT=>true, PDO::ATTR_ERRMODE=>true));	
		}
		catch (PDOException $error)
		{
			echo "Error:" . $error;
		}
	}
	//Inicar la Transacción de PDO
	private function iniciar()
	{
		$this->conexion->beginTransaction();
	}
	//Poner en Espera
	private function finalizar()
	{
		$this->conexion->commit();
	}
	//***Funciones Internas***********
	//Preparar y Ejecutar
	private function preparar($queryP)
	{
		 $this->preparado = $this->conexion->prepare($queryP);
		 $this->preparado->execute();
		 return $this->preparado;
	}
	//Obtener el ID de la Insercion
	private function id()
	{
		return $this->conexion->lastInsertId();
	}
	//***Fin Funciones Internas***********
	//Asociar a un Array
	protected function asociar($consulta)
	{
		return $consulta->fetch();
	}
	//Asociar Todo a un Array
	protected function asociarTodo($consulta)
	{
		return $consulta->fetchall(PDO::FETCH_ASSOC);
	}
	//public function consultarDatos($_tabla, $_campos=array("*"), $_filtro = "")
	public function consultarDatos($_tabla, $_campos=array(0=>"*"), $_filtro = "")
	{
		$_cadenaConsulta = "SELECT ";
		 //Determinar si son campos especificos o todos
		 if($_campos[0] == "*")
		 {
		 	$_cadenaConsulta .= "* ";
		 }
		 else
		 {
		 	//Cuando son campos especificos
		 	foreach ($_campos as $idCampo => $valorCampo)
		 	{
		 		$_cadenaConsulta .=  $valorCampo . ",";
		 	}
		 }
		 $_cadenaConsulta = substr($_cadenaConsulta,0,strlen($_cadenaConsulta)-1) . " ";
		 $_cadenaConsulta .= "FROM " . $_tabla . " ";
		 //Determinamos si hay condicion o filtro de busqueda
		 if($_filtro != "")
		 {
		 	$_cadenaConsulta .= " " . $_filtro;
		 }
		 $_cadenaConsulta;
		 $this->iniciar();
		 $_salida = $this->preparar($_cadenaConsulta);
		 $_salidaF = $this->asociarTodo($_salida);
		 $this->finalizar();
		 return $_salidaF;
		 
	}
	public function manual($_manual)
	{
		$_cadenaConsulta = $_manual;
		$this->iniciar();
		$_salida = $this->preparar($_cadenaConsulta);
		$_salidaF = $this->asociarTodo($_salida);
		$this->finalizar();
		return $_salidaF;
			
	}
	//public function insertarDatos($_tabla, $_post=array()) --> Retorna el ID
	public function insertarDatos($_tabla, $_post)
	{
		$_campos = $this->preparar("DESC " . $_tabla);
		$_campos = $this->asociarTodo($_campos);
		$_cadenaInsertar = "INSERT INTO " . $_tabla . "(";
		$_valores = "";
		//Recorrido por el DESC de la Tabla
		foreach ($_campos as $llaveCampos)
		{
			$_camposTabla[]= $llaveCampos['Field'];
		}
		//Recorrido por el POST enviado
		foreach ($_post as $llavePost => $valorPost)
		{
			$_indice = array_search($llavePost, $_camposTabla);
			if($_indice != false)
			{
				$_cadenaInsertar .= $llavePost . ", ";
				$_valores .= "'" . htmlspecialchars($valorPost, ENT_QUOTES, "UTF-8", TRUE) . "', ";
			}
		}
		$_cadenaInsertar = substr($_cadenaInsertar,0, strlen($_cadenaInsertar)-2);
		$_valores = substr($_valores,0, strlen($_valores)-2);
		$_cadenaInsertar .= ") VALUES (" . $_valores . ")";
		$this->iniciar();
		$this->preparar($_cadenaInsertar);
		$id= $this->id();
		$this->finalizar();
		return $id;
	}
	//public function actualizarDatos($tabla, $campos=array(campo=>valor), $condicion=array(campo=>valor)) --> Retorna numero de filas afectadas
	public function actualizarDatos($tabla, $campos, $condicion)
	{
		$_camposInternos = $this->preparar("DESC " . $tabla);
		$_camposInternos = $this->asociarTodo($_camposInternos);
		$_cadenaActualizar = "UPDATE " . $tabla . " SET ";
		$_valores = "";
		$_condicionInterna = "";
		//Recorrido por el DESC de la Tabla
		foreach ($_camposInternos as $llaveCamposInternos)
		{
			$_camposTabla[]= $llaveCamposInternos['Field'];
		}
		//Recorrido por el POST enviado
		foreach ($campos as $llaveCampos => $valorCampos)
		{
			$_indice = array_search($llaveCampos, $_camposTabla, true);
			if($_indice != false)
			{
				$_valores .= $llaveCampos . "='" . htmlspecialchars($valorCampos, ENT_QUOTES, "UTF-8", TRUE) . "', ";
			}
		}
		$_valores = substr($_valores,0, strlen($_valores)-2);
		$_cadenaActualizar .= $_valores . " WHERE ";
		//Hacemos Recorrido por la condición como array
		foreach ($condicion as $llaveCondicion => $valorCondicion)
		{
			$_condicionInterna .= $llaveCondicion . "='" . $valorCondicion . "'";
		}
		$_cadenaActualizar .= $_condicionInterna;
		$this->iniciar();
		$id = $this->preparar($_cadenaActualizar);
		$idx = $id->rowCount();
		$this->finalizar();
		return $idx;
	}
	//public function eliminarDatos($tabla, $condicion=array(campo=>valor))--> Retorna las filas afectadas
	public function eliminarDatos($tabla, $condicion)
	{
		$_cadenaEliminar = "DELETE FROM " . $tabla . " WHERE ";
		foreach ($condicion as $llaveCondicion => $valorEliminar)
		{
			$_cadenaEliminar .= $llaveCondicion . "='" . $valorEliminar . "'"; 
		}
		$this->iniciar();
		$retorno = $this->preparar($_cadenaEliminar);
		$_salida = $retorno->rowCount();
		$this->finalizar();
		return $_salida;
	}
}
?>