<?php
/*
 @Author Wilson Ramirez Zuluaga
 @date Apr 4, 2016
 @version 1.0
 @todo Metodos para Inicio de Sesion
 */
require_once 'gestionArchivos.php';


 abstract class seguridad
 {
 	//****
 	protected $cod1, $deco, $cook1, $_condicion;
 	//****
 	public function __construct()
 	{
 		$ip = $_SERVER['REMOTE_ADDR'];
		$arr = explode("\n", gestionArchivos::leer("usuario.txt"));
		array_pop($arr);
		if(in_array($ip, $arr))
		{
			
		}
		else
		{
			// "No está";
			gestionArchivos::escribir("usuario.txt", $ip);
		}

 	}
 	public function cargarSeguridad()
 	{
 		return $this;
 	}
 	public function codificar($_clave)
 	{
 		$this->cod1 = base64_encode($_clave);
 		return base64_encode($this->cod1);
 	}
 	public function decodificar($_clave)
 	{
 		$this->deco = base64_decode($_clave);
 		return base64_decode($this->deco);
 	}
 	//public function setCookie($_nombre, $_valor, $_tiempo = 30, $_ruta ="/")
 	public function setCookie($_nombre, $_valor, $_tiempo = 30, $_ruta ="/")
 	{
 		$_tiempo = $_tiempo*60;
 		setcookie($_nombre, base64_encode($_valor), time()+$_tiempo, $_ruta);
 		return $this;
 	}
 	public function getCookie($_nombre)
 	{
 		return base64_decode($_COOKIE[$_nombre]);
 	}
 	public function eliminarCookie($_nombre)
 	{
 		setcookie($_nombre, base64_encode("basura"), -1);
 		unset($_COOKIE[$_nombre]);
 	}
 	public function setSesion($_nombre, $_valor)
 	{
 		$_SESSION[$_nombre] = $this->codificar($_valor);
 		return $this;
 	}
 	public function getSesion($_nombre)
 	{
 		return base64_decode(base64_decode($_SESSION[$_nombre]));
 	}
 	public function cerrarSesion()
 	{
 		foreach ($_SESSION as $llaveSesiones =>$valorSesiones)
 		{
 			unset($_SESSION[$llaveSesiones]);
 		}
 		foreach ($_COOKIE as $llaveCookie => $valorCookie)
 		{
 			setcookie($llaveCookie, "", -1);
 			unset($_COOKIE[$llaveCookie]);
 		}
 		return 0;
 	}
 	//public function iniciarSesion($_conexion, $_tabla, $_post, $_direccion)
 	public function iniciarSesion($_conexion, $_tabla, $_post)
 	{
 		$this->_condicion .= "WHERE ";
 		foreach ($_post as $llavePost => $valorPost)
 		{
 			$this->_condicion .= $llavePost . "='" . $valorPost . "' AND ";
 		}
 		$this->_condicion = substr($this->_condicion, 0 , strlen($this->_condicion)-5);
 		return $salida = $_conexion->consultarDatos($_tabla, array(0=>"*"), $this->_condicion);
 	
 	}
 	public function generarClave()
 	{
 		//Generar clave longitud 6 especiales numeros y letras
 		$_claveNueva = "";
 		$_claveNueva .= chr(rand(97, 122));
 		$_claveNueva .= chr(rand(65, 90));
 		$_claveNueva .= chr(rand(65, 122));
 		$_claveNueva .= chr(rand(58, 64));
 		$_claveNueva .= chr(rand(48, 57));
 		$_claveNueva .= chr(rand(58, 64));
 		$_claveNueva .= chr(rand(35, 38));
 		return $this->codificar($_claveNueva);
 	}
 	public function validarClave($_clave)
 	{
 		$longitud = strlen($_clave);
 		$CantidadNumeros = 0;
 		$cantidadMayuscula = 0;
 		$cantidadMinuscula = 0;
 		if($longitud>=6)
 		{
 			for ($i=0; $i<$longitud; $i++)
 			{
 				$_subCadena= substr($_clave, $i,1) . "<br>";
 				$ascii = ord($_subCadena)."<br>";
 				if($ascii>= 48 && $ascii<=57)
 				{
 					
 					$CantidadNumeros+=1;
 				}
 				if($ascii>=65 && $ascii<=90)
 				{
 					$cantidadMayuscula +=1;
 				}
 				if($ascii>=97 && $ascii<=122)
 				{
 					$cantidadMinuscula +=1;
 				}
 				
 			}
 			 $_salida= ($cantidadMayuscula*$CantidadNumeros*$cantidadMinuscula);
 			if($_salida>0)
 			{
 				return true;
 			}
 			else
 			{
 				return false;
 			}
 		}
 	}
 }
 ?>