<?php
/*
 @Author Wilson Ramirez Zuluaga
 @date Apr 6, 2016
 @version 1.0
 @todo Manejo de Archivos planos o formateados
 */
 abstract class gestionArchivos
 {
 	//public function escribir($_archivo, $_cadena, $_local="SI")
 	public function escribir($_archivo, $_cadena, $_local="SI")
 	{
 		$_contenido = "";
 		if($_local == "SI")
 		{
 			$_ruta = ROOTC . "archivos" . DS . $_archivo;
 		}
 		else
 		{
 			$_ruta = $_archivo;
 		}
 		$this->_hilo = fopen($_ruta, "a+");
 		filesize($_ruta);
 		if(filesize($_ruta)!=0)
 		{
 			 $_contenido .= fread($this->_hilo, filesize($_ruta));
 		}
 		$_contenido .= $_cadena . "\n";
 		fwrite($this->_hilo, $_contenido, strlen($_contenido));
 		fclose($this->_hilo);
 	}
 	//public function leer($_archivo, $_local="SI")
 	public function leer($_archivo, $_local="SI")
 	{
 		$_contenido = "";
 		if($_local == "SI")
 		{
 			$_ruta = ROOTC . "archivos" . DS . $_archivo;
 		}
 		else
 		{
 			$_ruta = $_archivo;
 		}
 		$this->_hilo = fopen($_ruta, "a+");
 		filesize($_ruta);
 		if(filesize($_ruta)!=0)
 		{
 			 $_contenido .= fread($this->_hilo, filesize($_ruta));
 		}
 		fclose($this->_hilo);
 		return $_contenido;
 	}
 }
?>